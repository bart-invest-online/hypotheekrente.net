<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hypotheekrente');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'AqLVFw1q&nK|Y8@rEwP:h[Z[[U3X O`/d%<{+jb&V4+gT|&ym;w:CqSvDYe`9HeI');
define('SECURE_AUTH_KEY',  'U%[+jz[]L&d~bKP{&@*Vi^S]j](*3bD~_|$ KnT0WBMMahF-UotanA$@-=R=#hN:');
define('LOGGED_IN_KEY',    '3w?{Rvd#K?sE-O_8uf/G(E|AUs+8#l;K131;ambsvKw26HWxxIM-++Yvw>olxt89');
define('NONCE_KEY',        'B%niv5j!|5A 2{vh@iHCIU|VZAs.Gix.Wo|. ?u8l%$|:I:p*Sa/hDD+i|kz5Y6d');
define('AUTH_SALT',        'DM;%-GPE%O?|]hIs|s?JuBDbN3V?+H$Bla5>ixvKrb%_PIMFeP5[d~!plU99G]2r');
define('SECURE_AUTH_SALT', '*S_qH0AN[Ugy&P H2JrDq)k2<-C@Ep0F-CU~-d2XJL~3!$-~E>D,+K !#h>OfQ)k');
define('LOGGED_IN_SALT',   ']rR4<Mk>vHF$A]%I|8`tx@OuCD0}te_8gVfM#[B]{=AnlY:H_dqlZ/m`YPI_5z(J');
define('NONCE_SALT',       'Yt8d`zYHP9+D(+U)-P`uN+bY|wP-}6S<9*5P|M41+3WCIx!8#aj@|mbR0SOq Gh3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'hypo_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
