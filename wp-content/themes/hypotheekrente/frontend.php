<?php
	/*
		Template Name: Frontend pagina
	*/
?>
<?php get_header(); ?>
<div class="row">
	<div class="col-md-6 col-sm-6">
		<div role="tabpanel">
		  <!-- Nav tabs -->
		  <ul class="nav nav-tabs" role="tablist">
		    <li role="presentation" class="active hidden-xs hidden-sm"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">hypotheek oversluiten</a></li>
		    <li role="presentation" class="hidden-xs hidden-sm"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">woning aankopen</a></li>
				<li role="presentation" class="dropdown hidden-lg hidden-md">
			    <a class="dropdown-toggle mobile-select" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
			      maak uw keuze <span class="caret"></span>
			    </a>
			    <ul class="dropdown-menu" role="menu">
						<li role="presentation"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">hypotheek oversluiten</a></li>
				    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">woning aankopen</a></li>
			    </ul>
			  </li>
		  </ul>
		  <!-- Tab panes -->
		  <div class="tab-content">
		    <div role="tabpanel" class="tab-pane fade in active" id="home">
					<div class="content">
						<h3>Oversluiten naar een goedkopere hypotheek met betere voorwaarden?</h3>
					</div>
					<div class="well">
						<h4>Vraag geheel vrijblijvend een overzicht met de scherpste rentes en interessante informatie omtrent oversluiten aan.</h4>
						<hr />
						<!-- Formulier -->
						<?php	include('lib/inc/main-form.php'); ?>
						<!-- End Formulier -->
					</div>
				</div>
		    <div role="tabpanel" class="tab-pane fade" id="profile">
					<div class="content">
						<h3>Oversluiten naar een goedkopere hypotheek met betere voorwaarden?</h3>
					</div>
					<div class="well">
						<h4>Vraag geheel vrijblijvend een overzicht met de scherpste rentes en interessante informatie omtrent oversluiten aan.</h4>
						<hr />
						<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
					</div>
				</div>
		  </div>
		</div>
	</div>

	<div class="col-md-6 col-sm-6">
		<div class="well-numbers">
			<div class="padded">
			<h3>Hypotheekrente overzicht:
				<?php
			    $datum = date("j F Y");
			    $dagvanweek = date("l");
			    $arraydag = array(
				    "Zondag",
				    "Maandag",
				    "Dinsdag",
				    "Woensdag",
				    "Donderdag",
				    "Vrijdag",
				    "Zaterdag"
			    );
			    $dagvanweek = $arraydag[date("w")];
			    $arraymaand = array(
				    "Januari",
				    "Februari",
				    "Maart",
				    "April",
				    "Mei",
				    "Juni",
				    "Juli",
				    "Augustus",
				    "September",
				    "Oktober",
				    "November",
				    "December"
			    );
			    $datum = date("j ") . $arraymaand
			    [date("n") - 1] . date(" Y");
			    echo "$datum";
		    ?>
			</h3>
			</div>
			<div class="table-responsive">
				<div style="background: #009deb; padding: 10px; color: #FFF;">
					Getoonde hypotheekrentes op basis van 10 jaar rentevast.
				</div>
				<!-- Hypotheek rente overzicht -->
				<?php	include('lib/inc/rente-overzicht.php'); ?>
				<!-- End Hypotheek rente overzicht -->
			</div>
			<a href="#" title="Vraag volledig hypotheek overzicht aan" class="btn btn-primary btn-lg btn-block bottom-button"><i class="icon-arrow-left"></i> Vraag volledig hypotheek overzicht aan</a>
		</div>
	</div>

</div>

<div class="row">
	<?php
		$args = array( 'posts_per_page' => 3, 'offset'=> 0, 'category' => 9 );
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<div class="col-md-4 col-sm-4">
			<article>
				<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
				<?php
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('post-thumbnail', array( 'class'	=> "img-responsive"));
				}
				?>
				</a>
			</article>
		</div>
	<?php endforeach;
	wp_reset_postdata();?>
</div>

<hr class="transparent" />

<div class="well">

	<div class="row">
		<div class="col-md-12">
			<h3>Veilig &amp; betrouwbaar</h3>
		</div>
	</div>
	<div class="row">
			<?php
				$args = array( 'posts_per_page' => 3, 'offset'=> 0, 'category' => 8 );
				$myposts = get_posts( $args );
				foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
				<div class="col-md-4">
					<article>
						<div class="media">
						  <div class="media-left">
								<?php
								if ( has_post_thumbnail() ) {
									the_post_thumbnail();
								}
								?>
						  </div>
						  <div class="media-body">
								<?php the_excerpt(); ?>
						  </div>
						</div>
					</article>
				</div>
			<?php endforeach;
			wp_reset_postdata();?>
	</div>

	<hr />

	<div class="row">

		<div class="col-md-6">
			<h3>Recente nieuwsberichten:</h3>
			<?php
				$args = array( 'posts_per_page' => 5, 'offset'=> 0, 'category' => 1 );
				$myposts = get_posts( $args );
				foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
				<article>
					<header>
						<h3 class="h4"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
					</header>
					<section class="blog-content">
						<?php the_excerpt(); ?>
					</section>
					<footer>
						<a href="<?php the_permalink() ?>" title="Lees meer" class="permalink">Lees meer</a>
					</footer>
				</article>
				<hr />
			<?php endforeach;
			wp_reset_postdata();?>
			<a href="" title="" class="btn btn-primary btn-lg btn-block">Bekijk het volledige nieuwsoverzicht <i class="icon-arrow-right"></i></a>
		</div>

		<div class="col-md-6">
			<h3>Recente beoordelingen Hypotheekrente.net</h3>
			<?php
				$args = array( 'posts_per_page' => 5, 'offset'=> 0, 'category' => 3 );
				$myposts = get_posts( $args );
				foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
				<div itemscope itemtype="http://schema.org/Review">
				<article>
					<header>
						<div itemprop="name">
							<h3 class="h4"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
						</div>
					</header>
					<section class="review-content">

						<div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating" class="rating">
						<!-- Review sterren -->
						<?php $value = get_post_meta($post->ID, 'Review', true);
							if($value == 'First Choice') {
								echo '<meta itemprop="worstRating" content="1"><span itemprop="ratingValue" style="display: none;">1</span>';
								echo '<i class="icon-star-two"></i>';
								echo '<span itemprop="bestRating" style="display: none;">5</span>';
							} elseif($value == 'Second Choice') {
								echo '<meta itemprop="worstRating" content="1"><span itemprop="ratingValue" style="display: none;">2</span>';
								echo '<i class="icon-star-two"></i><i class="icon-star-two"></i>';
								echo '<span itemprop="bestRating" style="display: none;">5</span>';
							} elseif($value == 'Third Choice') {
								echo '<meta itemprop="worstRating" content="1"><span itemprop="ratingValue" style="display: none;">3</span>';
								echo '<i class="icon-star-two"></i><i class="icon-star-two"></i><i class="icon-star-two"></i>';
								echo '<span itemprop="bestRating" style="display: none;">5</span>';
							} elseif($value == 'Fourth Choice') {
								echo '<meta itemprop="worstRating" content="1"><span itemprop="ratingValue" style="display: none;">4</span>';
								echo '<i class="icon-star-two"></i><i class="icon-star-two"></i><i class="icon-star-two"></i><i class="icon-star-two"></i>';
								echo '<span itemprop="bestRating" style="display: none;">5</span>';
							} elseif($value == 'Fifth Choice') {
								echo '<meta itemprop="worstRating" content="1"><span itemprop="ratingValue" style="display: none;">5</span>';
								echo '<i class="icon-star-two"></i><i class="icon-star-two"></i><i class="icon-star-two"></i><i class="icon-star-two"></i><i class="icon-star-two"></i>';
								echo '<span itemprop="bestRating" style="display: none;">5</span>';
							} else {
								echo 'Geen rating afgegeven';
							}
						?>
						</div>
						<div itemprop="itemReviewed" itemscope itemtype="http://schema.org/Thing" style="display: none;">
							<span itemprop="name">www.hypotheekrente.net</span>
						</div>
						<div itemprop="reviewBody"><?php the_content(); ?></div>

					</section>
					<footer>

						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="media">
								  <div class="media-left">
										<i class="icon-user light-blue big-icon"></i>
								  </div>
								  <div class="media-body">
										<div itemprop="author" itemscope itemtype="http://schema.org/Person">
											<span itemprop="name">Fa. Metz-Krullenboer</span><br />Rotterdam
										</div>
								  </div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="media">
								  <div class="media-left">
										<i class="icon-doctor light-blue big-icon"></i>
								  </div>
								  <div class="media-body">
										Fa. Metz-Krullenboer<br />Rotterdam
								  </div>
								</div>
							</div>
						</div>

					</footer>
				</article>
				</div>
				<hr />
			<?php endforeach;
			wp_reset_postdata();?>
		</div>

	</div><!-- end Row -->
</div>
<?php get_footer(); ?>
