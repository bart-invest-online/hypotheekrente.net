<?php get_header(); ?>
<!-- Row -->
<div class="well">
	<div class="row">
		<div class="col-md-9">
			<h3>Recente nieuwsberichten:</h3>
			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>

			<article>
				<header>
					<h3 class="h4"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
					<?php echo the_date( "m-d-Y", "<h5 class='date'>", "</h5>", $echo ); ?>
				</header>
				<section class="blog-content">
					<?php the_content(); ?>
				</section>
				<footer>
					<a href="<?php the_permalink() ?>" title="Lees meer" class="permalink">Lees meer</a>
				</footer>
			</article>
			<hr />
			<?php endwhile; ?>

			<nav class="page-nav">
				<p><?php posts_nav_link('&nbsp;&bull;&nbsp;'); ?></p>
			</nav>

			<?php else : ?>

			<article>
				<h3><?php _e('Not Found', 'h5'); ?></h3>
				<p><?php _e('Sorry, but the requested resource was not found on this site.', 'h5'); ?></p>
				<?php get_search_form(); ?>
			</article>

			<?php endif; ?>

		</div>
		<div class="col-md-3">
			<?php get_sidebar(); ?>
		</div>
	</div><!-- end Row -->
</div>
<?php get_footer(); ?>
