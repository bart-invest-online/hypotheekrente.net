<form method="get" action="<?php echo home_url('/'); ?>">
  <div class="form-group">
    <div class="input-group">
			<input type="text" class="form-control" id="s" name="s" value="<?php the_search_query(); ?>">
      <div class="input-group-addon"><input type="submit" value="Search" class="btn"></div>
    </div>
  </div>
</form>
