<?php get_header(); ?>

	<div class="row">
		<div class="col-md-5">
			<div class="well">
				<!-- Formulier -->
				<?php	include('lib/inc/main-form.php'); ?>
				<!-- End Formulier -->
			</div>
		</div>
		<div class="col-md-7">
			<div class="well">
					<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
					} ?>
					<hr />
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<div class="section">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<header>
								<h1 class="h3"><?php the_title(); ?></h1>
							</header>
							<section>
								<?php the_content(); ?>
							</section>
							<footer>
								<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
							</footer>
						</article>
					</div>

					<?php endwhile; else: ?>

					<div class="section">
						<article>
							<p><?php _e('Sorry, no posts matched your criteria.', 'h5'); ?></p>
						</article>
					</div>

				<?php endif; ?>
			</div>
		</div>
	</div><!-- end Row -->

<?php get_footer(); ?>
