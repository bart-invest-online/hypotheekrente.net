	<div class="well-footer">
		<!-- Row -->
		<div class="row">
			<footer id="footer">
				<div class="col-md-4 col-sm-4">
					<h4>Meer weten?</h4>
					<?php wp_nav_menu( array( 'container' => 'false', 'container_class' => 'footer-menu', 'theme_location' => 'meerweten' ) ); ?>
					<h4>Aanbevolen links</h4>
					<?php wp_nav_menu( array( 'container' => 'false', 'container_class' => 'footer-menu', 'theme_location' => 'aanbevolen' ) ); ?>
				</div>
				<div class="col-md-4 col-sm-4">
					<h4>Hulp nodig?</h4>
					<?php wp_nav_menu( array( 'container' => 'false', 'container_class' => 'footer-menu', 'theme_location' => 'hulpnodig' ) ); ?>
					<h4>Direct uw hypotheek berekenen</h4>
					<?php wp_nav_menu( array( 'container' => 'false', 'container_class' => 'footer-menu', 'theme_location' => 'berekenen' ) ); ?>
				</div>
				<div class="col-md-4 col-sm-4">
					<h4>Contact opnemen</h4>
					<p>Hypotheekrente.net<br />Prins Willem Alexanderlaan 320<br />3921 NZ, Apeldoorn<br /><br />T: 020 - 123 456 78<br />E: <a href="mailto:info@hypotheekrente.net" title="E-mail adres">info@hypotheekrente.net</a></p>
				</div>
			</footer>
		</div><!-- end Row -->
	</div>
	<p class="copyright-info">&copy; copyright <?php echo date('Y'); ?> <a href="<?php echo home_url('/'); ?>"><?php bloginfo('name'); ?></a></p>
</div>
<?php wp_footer(); ?>
<!-- JS -->
<script src="<?php echo get_template_directory_uri(); ?>/lib/js/app.min.js"></script>
</body>
</html>
