<?php

// =========================================================================
// Head opschonen
// =========================================================================
remove_action('wp_head', 'rsd_link'); // remove really simple discovery link
remove_action('wp_head', 'wp_generator'); // remove wordpress version
remove_action('wp_head', 'feed_links', 2); // remove rss feed links (make sure you add them in yourself if youre using feedblitz or an rss service)
remove_action('wp_head', 'feed_links_extra', 3); // removes all extra rss feed links
remove_action('wp_head', 'index_rel_link'); // remove link to index page
remove_action('wp_head', 'wlwmanifest_link'); // remove wlwmanifest.xml (needed to support windows live writer)
remove_action('wp_head', 'start_post_rel_link', 10, 0); // remove random post link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // remove parent post link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // remove the next and previous post links
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 );

function thema_remove_recent_comments_style() {
  global $wp_widget_factory;
  remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}
add_action( 'widgets_init', 'thema_remove_recent_comments_style' );

// Tablepress stylesheet niet laden:
add_filter( 'tablepress_use_default_css', '__return_false' );

// Opschonen menu classes
add_filter( "nav_menu_css_class", "nav_menu_item_parent_classing", 10, 2 );
function nav_class_filter( $var ) {
	return is_array($var) ? array_intersect($var, array('hidden-sm', 'hidden-md', 'dropdown', 'show-xs', 'show-sm', 'nav', 'navbar-nav', 'has-submenu', 'current-menu-item', 'current-menu-parent', 'current-menu-ancestor')) : '';
}
add_filter('nav_menu_css_class', 'nav_class_filter', 100, 1);
function nav_id_filter( $id, $item ) {
	return 'nav-'.strtolower( str_replace( ' ','-',$item->title ) );
}
add_filter( 'nav_menu_item_id', 'nav_id_filter', 10, 2 );
add_filter( 'body_class', 'wpse15850_body_class', 10, 2 );
// Opschonen body classes dmv. whitelist
function wpse15850_body_class( $wp_classes, $extra_classes ) {
		// List of the only WP generated classes allowed
		$whitelist = array( 'woocommerce', 'page', 'shop', 'portfolio', 'home', 'error404' );
		// Filter the body classes
		$wp_classes = array_intersect( $wp_classes, $whitelist );
		// Add the extra classes back untouched
		return array_merge( $wp_classes, (array) $extra_classes );
}

// Featured image support
add_theme_support( 'post-thumbnails' );

// enable widgetized sidebars
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name'=> __('Widgets Sidebar', 'h5'),
		'id' => 'widgets_sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>'
	));
}

function register_my_menus() {
  register_nav_menus(
    array(
      'primary' => __( 'Hoofd Menu' ),
      'meerweten' => __( 'Meer weten Menu' ),
      'aanbevolen' => __( 'Aanbevolen Menu' ),
      'berekenen' => __( 'Direct berekenen Menu' ),
      'hulpnodig' => __( 'Hulp nodig Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );
add_theme_support( 'menus' );

function vantage_child_gravity_init_scripts() {
   return true;
}
add_filter("gform_init_scripts_footer", "vantage_child_gravity_init_scripts");

/* Gravity form actions */
add_filter( 'gform_next_button', 'input_to_button', 10, 2 );
add_filter( 'gform_previous_button', 'input_to_button', 10, 2 );
add_filter( 'gform_submit_button', 'input_to_button', 10, 2 );
function input_to_button( $button, $form ) {
    $dom = new DOMDocument();
    $dom->loadHTML( $button );
    $input = $dom->getElementsByTagName( 'input' )->item(0);
    $new_button = $dom->createElement( 'button' );
    $button_span = $dom->createElement( 'span', $input->getAttribute( 'value' ) );
    $new_button->appendChild( $button_span );
    $input->removeAttribute( 'value' );
    foreach( $input->attributes as $attribute ) {
        $new_button->setAttribute( $attribute->name, $attribute->value );
    }
    $new_button->setAttribute('class', 'btn btn-secondary');
    $input->parentNode->replaceChild( $new_button, $input );
    return $dom->saveHtml( $new_button );
}

add_filter("gform_submit_button_1", "form_submit_button", 10, 2);
function form_submit_button($button, $form){
  return "<button class='btn btn-secondary' id='gform_submit_button_{$form["id"]}'><span>Offerte aanvragen<i class='icon-arrow-right'></i></span></button>";
}

add_filter( 'gform_confirmation_anchor', '__return_false' );
add_filter( 'gform_ajax_spinner_url', 'tgm_io_custom_gforms_spinner' );
/**
 * Changes the default Gravity Forms AJAX spinner.
 *
 * @since 1.0.0
 *
 * @param string $src  The default spinner URL.
 * @return string $src The new spinner URL.
 */
function tgm_io_custom_gforms_spinner( $src ) {
    return '';
}

wp_deregister_script('jquery');
wp_register_script('jquery', '', '', '', true);

class wp_bootstrap_navwalker extends Walker_Nav_Menu {
	/**
		* @see Walker::start_lvl()
		* @since 3.0.0
		*
		* @param string $output Passed by reference. Used to append additional content.
		* @param int $depth Depth of page. Used for padding.
		*/
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat( "\t", $depth );
		$output .= "\n$indent<ul role=\"menu\" class=\" dropdown-menu\">\n";
	}
	/**
		* @see Walker::start_el()
		* @since 3.0.0
		*
		* @param string $output Passed by reference. Used to append additional content.
		* @param object $item Menu item data object.
		* @param int $depth Depth of menu item. Used for padding.
		* @param int $current_page Menu item ID.
		* @param object $args
		*/
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		/**
			* Dividers, Headers or Disabled
			* =============================
			* Determine whether the item is a Divider, Header, Disabled or regular
			* menu item. To prevent errors we use the strcasecmp() function to so a
			* comparison that is not case sensitive. The strcasecmp() function returns
			* a 0 if the strings are equal.
			*/
		if ( strcasecmp( $item->attr_title, 'divider' ) == 0 && $depth === 1 ) {
			$output .= $indent . '<li role="presentation" class="divider">';
		} else if ( strcasecmp( $item->title, 'divider') == 0 && $depth === 1 ) {
			$output .= $indent . '<li role="presentation" class="divider">';
		} else if ( strcasecmp( $item->attr_title, 'dropdown-header') == 0 && $depth === 1 ) {
			$output .= $indent . '<li role="presentation" class="dropdown-header">' . esc_attr( $item->title );
		} else if ( strcasecmp($item->attr_title, 'disabled' ) == 0 ) {
			$output .= $indent . '<li role="presentation" class="disabled"><a href="#">' . esc_attr( $item->title ) . '</a>';
		} else {
			$class_names = $value = '';


			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$classes[] = 'menu-item-' . $item->ID;
			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );

			if ( $args->has_children )
				$class_names .= '';
			if ( in_array( 'current-menu-item', $classes ) )
				$class_names .= ' active';
			$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
			$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
			$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
			$output .= $indent . '<li' . $id . $value . $class_names .'>';
			$atts = array();
			$atts['title']  = ! empty( $item->title )	? $item->title	: '';
			$atts['target'] = ! empty( $item->target )	? $item->target	: '';
			$atts['rel']    = ! empty( $item->xfn )		? $item->xfn	: '';
			// If item has_children add atts to a.
			if ( $args->has_children && $depth === 0 ) {
				$atts['href']   				= $item->url;
				$atts['data-toggle']		= 'dropdown';
				$atts['data-hover']			= 'dropdown';
				$atts['class']					= 'dropdown-toggle';
				$atts['aria-haspopup']	= 'true';
			} else {
				$atts['href'] = ! empty( $item->url ) ? $item->url : '';
			}
			$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );
			$attributes = '';
			foreach ( $atts as $attr => $value ) {
				if ( ! empty( $value ) ) {
					$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
					$attributes .= ' ' . $attr . '="' . $value . '"';
				}
			}
			$item_output = $args->before;
			/*
				* Glyphicons
				* ===========
				* Since the the menu item is NOT a Divider or Header we check the see
				* if there is a value in the attr_title property. If the attr_title
				* property is NOT null we apply it as the class name for the glyphicon.
				*/
			if ( ! empty( $item->attr_title ) )
				$item_output .= '<a'. $attributes .'><span class="glyphicon ' . esc_attr( $item->attr_title ) . '"></span>&nbsp;';
			else
				$item_output .= '<a'. $attributes .'>';
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output .= ( $args->has_children && 0 === $depth ) ? ' <span class="caret"></span></a>' : '</a>';
			$item_output .= $args->after;
			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}
	}
	/**
		* Traverse elements to create list from elements.
		*
		* Display one element if the element doesn't have any children otherwise,
		* display the element and its children. Will only traverse up to the max
		* depth and no ignore elements under that depth.
		*
		* This method shouldn't be called directly, use the walk() method instead.
		*
		* @see Walker::start_el()
		* @since 2.5.0
		*
		* @param object $element Data object
		* @param array $children_elements List of elements to continue traversing.
		* @param int $max_depth Max depth to traverse.
		* @param int $depth Depth of current element.
		* @param array $args
		* @param string $output Passed by reference. Used to append additional content.
		* @return null Null on failure with no changes to parameters.
		*/
	public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
				if ( ! $element )
						return;
				$id_field = $this->db_fields['id'];
				// Display this element.
				if ( is_object( $args[0] ) )
						$args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );
				parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
		}
	/**
		* Menu Fallback
		* =============
		* If this function is assigned to the wp_nav_menu's fallback_cb variable
		* and a manu has not been assigned to the theme location in the WordPress
		* menu manager the function with display nothing to a non-logged in user,
		* and will add a link to the WordPress menu manager if logged in as an admin.
		*
		* @param array $args passed from the wp_nav_menu function.
		*
		*/
	public static function fallback( $args ) {
		if ( current_user_can( 'manage_options' ) ) {
			extract( $args );
			$fb_output = null;
			if ( $container ) {
				$fb_output = '<' . $container;
				if ( $container_id )
					$fb_output .= ' id="' . $container_id . '"';
				if ( $container_class )
					$fb_output .= ' class="' . $container_class . '"';
				$fb_output .= '>';
			}
			$fb_output .= '<ul';
			if ( $menu_id )
				$fb_output .= ' id="' . $menu_id . '"';
			if ( $menu_class )
				$fb_output .= ' class="' . $menu_class . '"';
			$fb_output .= '>';
			$fb_output .= '<li><a href="' . admin_url( 'nav-menus.php' ) . '">Add a menu</a></li>';
			$fb_output .= '</ul>';
			if ( $container )
				$fb_output .= '</' . $container . '>';
			echo $fb_output;
		}
	}
}

class T5_Nav_Menu_Walker_Simple extends Walker_Nav_Menu
{
	/**
	 * Start the element output.
	 *
	 * @param  string $output Passed by reference. Used to append additional content.
	 * @param  object $item   Menu item data object.
	 * @param  int $depth     Depth of menu item. May be used for padding.
	 * @param  array $args    Additional strings.
	 * @return void
	 */
	public function start_el( &$output, $item, $depth, $args )
	{
		$output     .= '<li class="navbar-text pull-left">';
		$attributes  = '';

		! empty ( $item->attr_title )
			// Avoid redundant titles
			and $item->attr_title !== $item->title
			and $attributes .= ' title="' . esc_attr( $item->attr_title ) .'"';

		! empty ( $item->url )
			and $attributes .= ' href="' . esc_attr( $item->url ) .'"';

		$attributes  = trim( $attributes );
		$title       = apply_filters( 'the_title', $item->title, $item->ID );
		$item_output = "$args->before<a $attributes>$args->link_before$title</a>"
						. "$args->link_after$args->after";

		// Since $output is called by reference we don't need to return anything.
		$output .= apply_filters(
			'walker_nav_menu_start_el'
			,   $item_output
			,   $item
			,   $depth
			,   $args
		);
	}

	/**
	 * @see Walker::start_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function start_lvl( &$output )
	{
		$output .= '<ul class="nav pull-left hidden-lg hidden-md">';
	}

	/**
	 * @see Walker::end_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function end_lvl( &$output )
	{
		$output .= '</ul>';
	}

	/**
	 * @see Walker::end_el()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	function end_el( &$output )
	{
		$output .= '</li>';
	}
}

add_action("gform_enqueue_scripts", "deregister_scripts");

	function deregister_scripts(){

wp_deregister_script("jquery"); }

// add_action("gform_post_submission", "set_post_content", 10, 2);
//   function set_post_content($entry, $form){
//     // Lets get the IDs of the relevant fields and prepare an email message
//     //$message = print_r($entry, true);
//     // In case any of our lines are larger than 70 characters, we should use wordwrap()
//     //$message = wordwrap($message, 70);
//     // Send
//     // mail('bart.peperkamp@me.com', 'Getting the Gravity Form Field IDs', $message);
//     function post_to_url($url, $data) {
//       $fields = '';
//       foreach($data as $key => $value) {
//       $fields .= $key . '=' . $value . '&';
//     }
//     rtrim($fields, '&');
//     $post = curl_init();
//     curl_setopt($post, CURLOPT_URL, $url);
//     curl_setopt($post, CURLOPT_POST, count($data));
//     curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
//     curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
//     $result = curl_exec($post);
//     curl_close($post);
//     }
//     if($form["id"] == 1){//Join Our Mailing List
//       $data = array(
//       "first_name"  =>     $entry["1.3"],
//       "formName"    =>     "test-formulier"
//     );
//     post_to_url("http://192.168.100.127:7002", $data);
//   }
// }


// add_action("gform_post_submission_1", "set_post_content", 10, 2);
//
// function set_post_content($entry, $form)
// {
// 	//Gravity Forms has validated the data
// 	//Our Custom Form Submitted via PHP will go here
//
// 	// Lets get the IDs of the relevant fields and prepare an email message
// 	$message = print_r($entry, true);
//
// 	// In case any of our lines are larger than 70 characters, we should use wordwrap()
// 	$message = wordwrap($message, 70);
//
// 	// Send
// 	//mail('pop3.pranjal@gmail.com', 'Getting the Gravity Form Field IDs', $message);
//
// 	//Prepare to SEND DATA TO HOSTEDIVR.IN
// 	function post_to_url($url, $data)
// 	{
// 		$fields = '';
// 		foreach($data as $key => $value)
// 		{
// 			$fields .= $key . '=' . $value . '&';
// 		}
// 		rtrim($fields, '&');
//
// 		$post = curl_init();
//
// 		curl_setopt($post, CURLOPT_URL, $url);
// 		curl_setopt($post, CURLOPT_POST, count($data));
// 		curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
// 		curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
//
// 		$result = curl_exec($post);
//
// 		curl_close($post);
// 	}
//
// 	if($form["id"] == 1)
// 	{
// 		$data = array("looking_for" => $entry["1"],
// 				  "first_name" => $entry["3"]
// 				);
//
// 		post_to_url("http://192.168.100.127:7002", $data);
//  	}
// }
