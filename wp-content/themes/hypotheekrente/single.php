<?php
// Doorverwijzen naar verschillende templates aan de hand van de categorie
if(has_term('reviews', 'category', $post)) {
  get_template_part('single-template', 'reviews');
}
elseif(has_term('cat-2', 'category', $post)) {
  // deze categorie doet nog niks
  get_template_part('single-template', 'cat-2');
} else {
  get_template_part('single-template');
}
?>
