<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<!-- Hypotheekrente.net -->
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="application-name" content="<?php bloginfo('name'); ?>">

	<title><?php bloginfo('name'); ?><?php wp_title('|'); ?></title>

	<!-- Favicons -->
	<?php	include('lib/inc/favicons.php'); ?>
	<!-- End Favicons -->

	<!-- Styles -->
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>">

	<!-- DNS prefetch -->
	<link rel="dns-prefetch" href="//ssl.google-analytics.com">
	<link rel="dns-prefetch" href="//www.googleadservices.com">
	<link rel="dns-prefetch" href="//googleads.g.doubleclick.net">
	<link rel="dns-prefetch" href="//www.google.com">
	<link rel="dns-prefetch" href="//ajax.googleapis.com">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js" async></script>

	<!--[if lt IE 9]>
  	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/lib/css/ie.css">
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
<!-- Header -->
<header id="header">
	<div class="container">
		<!-- Row -->
		<div class="row">
				<div class="col-md-3 col-sm-8">
					<a href="/" title="<?php bloginfo('name'); ?>" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/img/hypotheekrente.png" width="200" height="50" title="Hypotheekrente.net" alt="Hypotheekrente logo" /></a>
				</div>
				<div class="col-md-6 hidden-sm">
					<form method="get" action="<?php echo home_url('/'); ?>" class="form-inline search-menu hidden-xs">
					  <div class="form-group">
					    <div class="input-group">
								<input type="text" class="form-control" id="s" name="s" value="<?php the_search_query(); ?>" placeholder="zoeken">
					    </div>
							<input type="submit" value="zoek" class="btn">
					  </div>
					</form>
				</div>
				<div class="col-md-3 col-sm-4 hidden-xs">
					<a href="http://www.goedkopehypotheek.nl/" title="<?php bloginfo('name'); ?>" target="_blank" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/img/banner-goedkopehypotheek.png" width="259" height="53" title="Goedkopehypotheek.nl" alt="Goedkope Hypotheek logo" class="img-responsive" /></a>
				</div>
		</div><!-- end Row -->
		<!-- Row -->
		<div class="row">
			<div class="col-md-12">
				<!-- Navigation -->
				<?php	include('lib/inc/navigation.php'); ?>
				<!-- End Navigation -->
			</div>
		</div><!-- end Row -->
	</div>
</header>
<!-- end Header -->
<div class="container">
