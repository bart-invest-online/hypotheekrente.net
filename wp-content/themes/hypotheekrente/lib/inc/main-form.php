<div class="form">
  <!-- Load form page -->
  <?php
    $page_id = 13;
    $page_data = get_page( $page_id );
    //echo '<h3>'. $page_data->post_title .'</h3>';
    //echo '<hr />';
    echo apply_filters('the_content', $page_data->post_content);
    echo '<hr style="border: 0px; clear: both" />';
  ?>
  <!-- end Load form page -->
</div>
