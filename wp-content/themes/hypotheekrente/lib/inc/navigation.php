<nav class="navbar navbar-default navbar-static-top">

  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-main-menu">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>

  <div class="collapse navbar-collapse" id="navbar-main-menu">
    <!-- Home icon -->
    <ul class="nav navbar-nav hidden-xs hidden-md hidden-sm">
      <li>
        <a href="<?php echo home_url('/'); ?>" title="Home"><i class="icon-home"></i></a>
      </li>
    </ul>
    <!-- end Home icon -->
    <!-- Hoofdmenu -->
    <?php
      wp_nav_menu( array(
        'menu'              => 'primary',
        'theme_location'    => 'primary',
        'depth'             => 2,
        'container'         => false,
        'menu_class'        => 'nav navbar-nav',
        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
        'walker'            => new wp_bootstrap_navwalker())
      );
    ?>
    <!-- end Hoofdmenu -->
    <ul class="nav navbar-nav navbar-right offer" style="margin-right: 0px;">
      <li>
        <a href="#" title="Home" class="offer-request" onclick="setFocus();">Offerte aanvragen <i class="icon-arrow-right"></i></a>
        <script>
        function setFocus() {
            var editor = $('#input_1_12');
            var value = editor.val();
            editor.val("");
            editor.focus();
            editor.val(value);
        }
        </script>
      </li>
    </ul>
  </div>

</nav>
