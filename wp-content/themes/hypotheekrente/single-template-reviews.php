<?php get_header(); ?>

	<div class="row">
		<div class="col-md-5">
			<div class="well">
			 Hier komt het formulier
			</div>
		</div>
		<div class="col-md-7">
			<div class="well">
			<?php if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('<p id="breadcrumbs">','</p>');
			} ?>
			<hr />
		<h1>Reviews!</h1>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<div class="section">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header>
						<h1 class="h3"><?php the_title(); ?></h1>
					</header>
					<section>
						<?php if (has_post_thumbnail()) the_post_thumbnail(); ?>

						<div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating" class="rating">
						<!-- Review sterren -->
						<?php $value = get_post_meta($post->ID, 'Review', true);
							if($value == 'First Choice') {
								echo '<meta itemprop="worstRating" content="1"><span itemprop="ratingValue" style="display: none;">1</span>';
								echo '<i class="icon-star-two"></i>';
								echo '<span itemprop="bestRating" style="display: none;">5</span>';
							} elseif($value == 'Second Choice') {
								echo '<meta itemprop="worstRating" content="1"><span itemprop="ratingValue" style="display: none;">2</span>';
								echo '<i class="icon-star-two"></i><i class="icon-star-two"></i>';
								echo '<span itemprop="bestRating" style="display: none;">5</span>';
							} elseif($value == 'Third Choice') {
								echo '<meta itemprop="worstRating" content="1"><span itemprop="ratingValue" style="display: none;">3</span>';
								echo '<i class="icon-star-two"></i><i class="icon-star-two"></i><i class="icon-star-two"></i>';
								echo '<span itemprop="bestRating" style="display: none;">5</span>';
							} elseif($value == 'Fourth Choice') {
								echo '<meta itemprop="worstRating" content="1"><span itemprop="ratingValue" style="display: none;">4</span>';
								echo '<i class="icon-star-two"></i><i class="icon-star-two"></i><i class="icon-star-two"></i><i class="icon-star-two"></i>';
								echo '<span itemprop="bestRating" style="display: none;">5</span>';
							} elseif($value == 'Fifth Choice') {
								echo '<meta itemprop="worstRating" content="1"><span itemprop="ratingValue" style="display: none;">5</span>';
								echo '<i class="icon-star-two"></i><i class="icon-star-two"></i><i class="icon-star-two"></i><i class="icon-star-two"></i><i class="icon-star-two"></i>';
								echo '<span itemprop="bestRating" style="display: none;">5</span>';
							} else {
								echo 'Geen rating afgegeven';
							}
						?>
						</div>

						<?php the_content(); ?>
					</section>
					<footer>
						<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
						<?php the_tags( '<p>Tags: ', ', ', '</p>'); ?>
					</footer>
				</article>

				<nav class="page-nav">
					<p><?php previous_post_link('%link', 'Vorige', TRUE); ?> &bull; <?php next_post_link( '%link', 'Volgende', TRUE ); ?></p>
				</nav>
			</div>

		<?php endwhile; else: ?>

			<div class="section">
				<article>
					<p><?php _e('Sorry, no posts matched your criteria.', 'h5'); ?></p>
				</article>
			</div>

		<?php endif; ?>
			</div>
		</div>
	</div><!-- end Row -->

<?php get_footer(); ?>
