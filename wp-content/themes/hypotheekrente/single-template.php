<?php get_header(); ?>
<div class="row">

	<div class="col-md-5">
		<div class="well">
			<h4>Vraag geheel vrijblijvend een overzicht met de scherpste rentes en interessante informatie omtrent oversluiten aan.</h4>
			<hr />
			<div class="form">
				<!-- Load form page -->
				<?php
					$page_id = 13;
					$page_data = get_page( $page_id );
					//echo '<h3>'. $page_data->post_title .'</h3>';
					//echo '<hr />';
					echo apply_filters('the_content', $page_data->post_content);
					echo '<hr style="border: 0px; clear: both" />';
				?>
				<div class="media" style="margin-top: -40px;">
					<div class="media-left">
						<i class="icon-user big-icon" style="color: #FFFFFF;"></i>
					</div>
					<div class="media-body">
						<p style="color: #FFFFFF; line-height: 1.2em;">51.563 mensen gingen<br /> u al voor!</p>
					</div>
				</div>
				<!-- end Load form page -->
			</div>
		</div>
	</div>

	<div class="col-md-7">
		<div class="well">
			<?php if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('<p id="breadcrumbs">','</p>');
			} ?>
			<hr />
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div class="section">
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header>
							<h1 class="h3"><?php the_title(); ?></h1>
						</header>
						<section>
							<?php the_content(); ?>
						</section>
						<footer>
							<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
							<?php the_tags( '<p>Tags: ', ', ', '</p>'); ?>
						</footer>
					</article>
					<hr />
					<nav class="page-nav">
						<p><?php previous_post_link('%link', 'Vorige', TRUE); ?> &bull; <?php next_post_link( '%link', 'Volgende', TRUE ); ?></p>
					</nav>
				</div>

			<?php endwhile; else: ?>

				<div class="section">
					<article>
						<p><?php _e('Sorry, no posts matched your criteria.', 'h5'); ?></p>
					</article>
				</div>

			<?php endif; ?>
		</div>
	</div>

</div>

<?php get_footer(); ?>
