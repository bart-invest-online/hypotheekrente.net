<?php get_header(); ?>
	<div class="row">
		<div class="col-md-5">
			<div class="well">
				<!-- Formulier -->
				<?php	include('lib/inc/main-form.php'); ?>
				<!-- End Formulier -->
			</div>
		</div>
		<div class="col-md-7">
			<div class="well">
			<?php if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('<p id="breadcrumbs">','</p>');
			} ?>
			<hr />
		<div class="section">

			<?php if (have_posts()) : ?>

				<?php $post = $posts[0]; // hack: set $post so that the_date() works ?>
				<?php if (is_category()) : ?>
				<h1 class="h3"><?php single_cat_title(); ?></h1>

				<?php elseif(is_tag()) : ?>
				<h1 class="h3"><?php _e('Posts tagged as', 'h5'); ?> <?php single_tag_title(); ?></h1>

				<?php elseif (is_day()) : ?>
				<h1 class="h3"><?php _e('Archive for', 'h5'); ?> <?php the_time('F jS, Y'); ?></h1>

				<?php elseif (is_month()) : ?>
				<h1 class="h3"><?php _e('Archive for', 'h5'); ?> <?php the_time('F, Y'); ?></h1>

				<?php elseif (is_year()) : ?>
				<h1 class="h3"><?php _e('Archive for', 'h5'); ?> <?php the_time('Y'); ?></h1>

				<?php elseif (is_author()) : ?>
				<h1 class="h3"><?php _e('Author Archive', 'h5'); ?></h1>

				<?php elseif (isset($_GET['paged']) && !empty($_GET['paged'])) : ?>
				<h1 class="h3"><?php _e('Blog Archives', 'h5'); ?></h1>

			<?php endif; ?>

			<?php while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header>
						<h2 class="h4"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					</header>
					<section>
						<?php the_excerpt(); ?>
					</section>
				</article>

				<?php endwhile; ?>

				<nav class="page-nav">
					<p><?php posts_nav_link('&nbsp;&bull;&nbsp;'); ?></p>
				</nav>

				<?php else : ?>

				<article>
					<h2 class="h4"><?php _e('Not Found', 'h5'); ?></h2>
					<p><?php _e('Sorry, but the requested resource was not found on this site.', 'h5'); ?></p>
					<?php get_search_form(); ?>
				</article>

				<?php endif; ?>

			</div>
		</div>
		</div>
	</div><!-- end Row -->

<?php get_footer(); ?>
